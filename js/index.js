import * as PL from './modules/photoloader.js';
import * as gallery from './modules/gallery.js';
import * as light from "./modules/lightbox";

$(document).ready(function() {
    PL.init("https://webetu.iutnc.univ-lorraine.fr");
    gallery.init("photobox-gallery");

    $('#load_gallery').click(function () {
        gallery.load();
    });
    $('#previous').click(function () {
        gallery.previous();
    });

    $('#photobox-gallery').click(function (e) {
        console.log(e);
        light.setImage(e.target);
        light.ouvrir();
    });

    $('#close').click(function () {
        light.fermer();
    });

    $('#prevPhoto').click(function () {
        light.plusSlides(-1);
    });

    $('#nextPhoto').click(function () {
        light.plusSlides(1);
    });

    $('#next').click(function () {
        gallery.next();
    });
});