let slideIndex = 1;

export function ouvrir() {
    $('#lightbox_container').css("display","block");
}

export function fermer() {
    $('#lightbox_container').css("display","none");
}

export function setImage(target){
    let img = $("#lightbox_full_img");
    let titre = $("#lightbox_title");
    img.attr("src",target.src);
    titre[0].innerHTML = target.nextElementSibling.innerHTML;
}

export function plusSlides(n) {
    showSlides(slideIndex += n);
}

function showSlides(n) {
    let slides = document.getElementsByClassName("vignette");
    if (n > slides.length) {
        slideIndex = 1;
    }
    if (n < 1) {
        slideIndex = slides.length;
    }
    setImage(slides[slideIndex-1].children[0]);
}