import * as photoloader from './photoloader.js';

export function init(idgal){
    id = idgal;
    page=1;
}

let page;
let id;
let nextLink;
let prevLink;

export function load(){

    photoloader.load("/www/canals5/photobox/photos/?offset=0&size=10").then(function(response){
        let data = response.data;
        nextLink=data.links.next;
        prevLink=data.links.prev;
        console.log(nextLink);

        let tab = data['photos'];
        tab.forEach(function(item){

            let photo = item['photo'];
            let gallery = document.querySelector("#photobox-gallery");

            let div = document.createElement("div");
            div.setAttribute("class","vignette");

            let img = document.createElement("img");
            img.setAttribute("data-img","https://webetu.iutnc.univ-lorraine.fr" + photo.original.href);
            img.setAttribute("data-uri","https://webetu.iutnc.univ-lorraine.fr/www/canals5/photobox/photos/" +photo.id);
            img.setAttribute("src","https://webetu.iutnc.univ-lorraine.fr" + photo.thumbnail.href);

            let titre = document.createElement("div");
            let t = document.createTextNode(photo.titre);

            titre.appendChild(t);
            div.appendChild(img);
            div.appendChild(titre);
            gallery.appendChild(div);
            document.querySelector("#gallery").appendChild(gallery);
        });

    });
}



export function chargement() {

    photoloader.load("/www/canals5/photobox/photos/?offset=" + page * 9 + "&size=9").then(function(response){
        let data = response.data;
        nextLink=data.links.next;
        prevLink=data.links.prev;
        console.log(nextLink);

        let tab = data['photos'];
        tab.forEach(function(item){

            let photo = item['photo'];
            let gallery = document.querySelector("#photobox-gallery");

            let div = document.createElement("div");
            div.setAttribute("class","vignette");

            let img = document.createElement("img");
            img.setAttribute("data-img","https://webetu.iutnc.univ-lorraine.fr" + photo.original.href);
            img.setAttribute("data-uri","https://webetu.iutnc.univ-lorraine.fr/www/canals5/photobox/photos/" +photo.id);
            img.setAttribute("src","https://webetu.iutnc.univ-lorraine.fr" + photo.thumbnail.href);

            let titre = document.createElement("div");
            let t = document.createTextNode(photo.titre);

            titre.appendChild(t);
            div.appendChild(img);
            div.appendChild(titre);
            gallery.appendChild(div);
            document.querySelector("#gallery").appendChild(gallery);
        });

    });
}


export function next(){
    page++;
    chargement();
}

export function previous(){
    if(page > 1)
        page--;
    chargement();
}