let server_url=null;

export function init(url){
    server_url=url;
}

export function load(uri){
    return axios.get(server_url+uri).catch(err => alert(err))
}